package shivam.intent_implicit;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Call(View v)
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 123);
        }
        else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
        {
            Intent i = new Intent();
            i.setAction(Intent.ACTION_CALL);
            i.setData(Uri.parse("tel:+918106424656"));
            startActivity(i);
        }
    }
    public void SMS(View v)
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 123);
        }
        else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED)
        {
            Intent i = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:XXXXXXXXXX")); //XXXXXXXXXX is number
            i.putExtra("sms_body", "XXXXXXXXXX is supposed to be a phone number.");
            startActivity(i);
        }
    }
    public void Web(View v)
    {
        Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.google.co.in"));
        startActivity(i);
    }
    public void Email(View v)
    {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL,new String[]{"jha.shivam3@gmail.com","jha.shivam3@yahoo.co.in"});
        i.putExtra(Intent.EXTRA_BCC,new String[]{"jha.shivam3@gmail.com"});
        i.putExtra(Intent.EXTRA_CC,new String[]{"jha.shivam3@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT,"Test");
        i.putExtra(Intent.EXTRA_TEXT,"Hey this message is generated programatically.");
        Intent.createChooser(i,"Gmail");
        startActivity(i);
    }
    public void Share(View v)
    {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT,"Sharing plain text");
        startActivity(i);
    }
}
